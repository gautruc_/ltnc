#include <SDL.h>
#include "hangman.h"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <ctime>
#include <vector>
#include <chrono>

using namespace std;

string getLowerCaseWord(const string& word){
    string res = word;
    int sz = word.size();
    for(int i = 0; i < sz; i++){
        res[i]= tolower(word[i]);
    return res;
    }
}

string chooseWord(const char* fileName){
    vector<string> wordList;
    ifstream file(fileName);
    if(file.is_open()){
        string word;
        while(file >> word){
            wordList.push_back(word);
        }
     file.close();
     chrono::milliseconds(500);
    }
    if (wordList.size() > 0) {
        int randomIndex = rand() % wordList.size();
        return getLowerCaseWord(wordList[randomIndex]);
    }else return "";
}

bool check(const string& word, char c){
    return word.find_first_of(c) != string::npos;
}

string updateWord(string& GuessWord, const string word, char guess){
    for(int i=0; i < word.length(); i++){
        if(guess==word[i]){
            GuessWord[i] = guess;
        }
    }
    return GuessWord;
}


