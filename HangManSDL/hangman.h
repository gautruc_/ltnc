#ifndef HANGMAN_H
#define HANGMAN_H

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <cctype>
#include <ctime>
#include <vector>
#include <chrono>


using namespace std;

const char dataFile[] = "hman.txt";


string getLowerCaseWord(const string& word);
string chooseWord(const char* fileName);

bool check(const string& word, char c);
string updateWord(string& GuessWord, const string word, char guess);


#endif // HANGMAN_H
