#include <iostream>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <cctype>
#include <vector>
#include <fstream>
#include <chrono>

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include "SDL_utils.h"
#include "painters.h"
#include "hangman.h"

using namespace std;

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const string WINDOW_TITLE = "HangMan SDL";

int main(int argc, char* argv[])
{
    srand(time(0));
    SDL_Window* window;
    SDL_Renderer* renderer;
    initSDL(window, renderer);
    SDL_Texture* image = loadTexture("background.bmp", renderer);
    renderTexture1(image, renderer, 0, 0, 800, 600);

    TTF_Font* font = NULL;      font = TTF_OpenFont("Tandysoft.ttf", 21);
    TTF_Font* font1 = NULL;     font1 = TTF_OpenFont("ULTRA9.ttf", 21);

    if( font == NULL && font1 == NULL ){
        cout << "Font Error !" << endl;
    }

    SDL_Surface* start_game = TTF_RenderText_Solid(font, "START", RED_COLOR);
    SDL_Texture* title_startgame = SDL_CreateTextureFromSurface(renderer, start_game);
    SDL_Surface* exitgame = TTF_RenderText_Solid(font, "EXIT", RED_COLOR);
    SDL_Texture* title_exitgame = SDL_CreateTextureFromSurface(renderer, exitgame);

    renderTexture1(title_startgame, renderer, 350, 290, 100, 30);
    renderTexture1(title_exitgame, renderer, 370, 370, 50, 30);

    SDL_RenderPresent(renderer);

    SDL_Event event;
    SDL_PollEvent(&event);
    while(true){
        int chance = 7;
        const string picture[] = { "", "0.bmp", "1.bmp", "2.bmp", "3.bmp", "4.bmp", "5.bmp", "6.bmp" };

        if(SDL_WaitEvent(&event) == 0) continue;
        if(event.type == SDL_QUIT) break;
        if(event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) break;

        if(event.type == SDL_MOUSEBUTTONDOWN) {
            if(event.button.x >= 350 && event.button.x <= 450 && event.button.y >= 290 && event.button.y <= 320)
            {
                SDL_Texture* background     = loadTexture("gamebackground.bmp", renderer);
                SDL_Texture* apple          = loadTexture("apple.bmp", renderer);
                SDL_Texture* youlose        = loadTexture("You Lose.bmp", renderer);
                SDL_Texture* youwin         = loadTexture("You Win.bmp", renderer);

                if(background == nullptr || youlose == nullptr || youwin == nullptr || apple == nullptr)
                {
                    SDL_DestroyTexture(background);
                    SDL_DestroyTexture(apple);
                    SDL_DestroyTexture(youlose);
                    SDL_DestroyTexture(youwin);

                    quitSDL(window, renderer);
                }

                Painter painter(window, renderer);
                clearScreen(window, renderer, painter, background, apple, chance);

                bool running = true;
                SDL_RenderPresent(renderer);

                ////////////////////////////////

                string word      = chooseWord(dataFile);
                string GuessWord = string(word.length(), '-');
                srand(time(0));

                while(running && chance > 0 && word != GuessWord){

                    SDL_Surface* cword = TTF_RenderText_Solid(font1, GuessWord.c_str(), PURPLE_COLOR);
                    SDL_Texture* CWORD = SDL_CreateTextureFromSurface(renderer, cword);
                    renderTexture1(CWORD, renderer, 230, 450, 300, 100);
                    SDL_RenderPresent(renderer);

                    SDL_Event event2;
                    while(SDL_PollEvent(&event2)){

                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_a){

                                                if(!check(word,tolower('a'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }

                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'a').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                            };


                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_b){

                                                if(!check(word,tolower('b'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }

                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'b').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                };
                                            };

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_c){
                                                if(!check(word,tolower('c'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                }

                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'c').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                            };

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_d){
                                                if(!check(word,tolower('d'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }

                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'd').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };

                                            }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_e){
                                                if(!check(word,tolower('e'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                }

                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'e').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                };
                                            }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_f){
                                                if(!check(word,tolower('f'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'f').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                         if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_g){
                                                if(!check(word,tolower('g'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'g').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                         if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_h){
                                                if(!check(word,tolower('h'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'h').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                         if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_i){
                                                if(!check(word,tolower('i'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'i').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_j){
                                                if(!check(word,tolower('j'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'j').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_k){
                                                if(!check(word,tolower('k'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'k').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_l){
                                                if(!check(word,tolower('l'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'l').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_m){
                                                if(!check(word,tolower('m'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'm').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_n){
                                                if(!check(word,tolower('n'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'n').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_o){
                                                if(!check(word,tolower('o'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'o').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_p){
                                                if(!check(word,tolower('p'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'p').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_q){
                                                if(!check(word,tolower('q'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'q').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }
                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_r){
                                                if(!check(word,tolower('r'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'r').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_s){
                                                if(!check(word,tolower('s'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 's').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_t){
                                                if(!check(word,tolower('t'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 't').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_u){
                                                if(!check(word,tolower('u'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'u').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_v){
                                                if(!check(word,tolower('v'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'v').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_w){
                                                if(!check(word,tolower('w'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'w').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_x){
                                                if(!check(word,tolower('x'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'x').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_y){
                                                if(!check(word,tolower('y'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'y').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                                                }

                                        if (event2.type == SDL_KEYDOWN && event2.key.keysym.sym == SDLK_z){
                                                if(!check(word,tolower('z'))){
                                                chance--;
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_RenderPresent(renderer);
                                                    }
                                                else  {
                                                refreshScreen(window, renderer, painter, background, apple, chance);
                                                SDL_Surface* cword2 = TTF_RenderText_Solid(font1, updateWord(GuessWord, word, 'z').c_str(), WHITE_COLOR);
                                                SDL_Texture* CWORD2 = SDL_CreateTextureFromSurface(renderer, cword2);
                                                renderTexture1(CWORD2, renderer, 230, 450, 300, 100);
                                                SDL_RenderPresent(renderer);
                                                    };
                            }

                    }//END Event2
                }//END Running
                if(chance == 0){
                    renderTexture1(youlose, renderer, 0, 0, 800, 600);
                }
                else {
                    SDL_Delay(100);
                    renderTexture1(youwin, renderer, 0, 0, 800, 600);
                }

                SDL_RenderPresent(renderer);
                SDL_DestroyTexture(background);
                SDL_DestroyTexture(youlose);
                SDL_DestroyTexture(youwin);
                SDL_DestroyTexture(apple);

            }//END Start;
            else if (event.button.x >= 370 && event.button.x <= 420 && event.button.y >= 370 && event.button.y <= 420)
                quitSDL(window, renderer);
        }//End MOUSE.BUTTON

    }//End While(true);


    quitSDL(window, renderer);
    return 0;
}//END main;

